$PackageVersion = "3.9.8.15"

# 2023-06-18 Fabio A. Correa <facorread@gmail.com>
# Chocolatey Automatic Uninstaller cannot silently uninstall WeChat because the installation does
# not create a QuietUninstallString value in the Windows Registry. Additionally, Uninstall.exe
# does not sanitize its arguments, so /S gets passed to the cleanup program Au_.exe, which hangs.

# Because of this, we need to use AutoHotKey to uninstall WeChat.

# This directory contains a file .skipAutoUninstall to tell Chocolatey to skip the automatic uninstaller
# https://github.com/chocolatey/choco/issues/1257

Set-StrictMode -Version 3.0

$Global:ErrorActionPreference = 'Stop'
$Global:InformationPreference = 'Continue'
$Global:VerbosePreference     = 'Continue'
$PackageInstaller             = "WeChatSetup_$PackageVersion"

# The WeChat installer and uninstaller do not detect that an instance of WeChat is running.
# Let us ensure a clean uninstall.

Get-Process -Name WeChat, "$PackageInstaller" , 'Au_' -ErrorAction SilentlyContinue | Stop-Process

# Also, to ensure a clean uninstall, we need to do the manual uninstall.
# If we wanted to leave the task to Chocolatey automatic uninstall, we would need
# to leave AutoHotKey hanging. The testing platform does not like processes that hang.

$registryKeys = Get-UninstallRegistryKey -SoftwareName "WeChat"

$autoHotkeyArgs = @{
    FilePath     = Get-ChocolateyPath -PathType InstallPath | Join-Path -ChildPath "lib\autohotkey.portable\tools\AutoHotkey.exe"
    ArgumentList = Get-ChocolateyPath -PathType PackagePath | Join-Path -ChildPath "tools\uninstall.ahk"
    PassThru     = $true
}

$AutoHotKeyProcess = Start-Process @autoHotkeyArgs
Write-Verbose "Running $($AutoHotKeyProcess.ProcessName)"

foreach ($key in $registryKeys) {
    $packageArgs = @{
        PackageName                   = 'wechat'
        FileType                      = 'exe'
        File                          = $key.UninstallString
        SilentArgs                    = ''
        UseOnlyPackageSilentArguments = $true
    }
    Uninstall-ChocolateyPackage @packageArgs
}

Wait-Process -InputObject $AutoHotKeyProcess -ErrorAction SilentlyContinue
Write-Verbose "Process $($AutoHotKeyProcess.ProcessName): HasExited $($AutoHotKeyProcess.HasExited) ExitCode $($AutoHotKeyProcess.ExitCode)"
