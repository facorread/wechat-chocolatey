package_version := "3.9.8.15"

; #ErrorStdOut             ; Best to keep this one disabled; it silences some errors.
; #Warn                    ; Enable warnings to assist with detecting common errors.
SendMode "Input"           ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir A_ScriptDir  ; Ensures a consistent starting directory.

close_all_instances(process_executable) {
    process_string := Format("ahk_exe {1}", process_executable)
    Loop 600 {
        If WinExist(process_string) {
            WinClose(process_string, , 600)
            Sleep(1000)
        } Else {
            Break
        }
    }
    Loop 600 {
        If ProcessExist(process_executable) {
            ProcessClose(process_executable)
            Sleep(1000)
        } Else {
            Break
        }
    }
    If ProcessExist(process_executable) {
        Throw("Please close all instances of " process_executable " before running choco")
    }
}

; https://www.autohotkey.com/docs/v2/lib/TrayTip.htm
hide_tray_tip() {
    TrayTip  ; Attempt to hide it the normal way.
    if SubStr(A_OSVersion,1,3) = "10." {
        A_IconHidden := true
        Sleep 200  ; It may be necessary to adjust this sleep.
        A_IconHidden := false
    }
}

patient_process_close(process_executable) {
    ProcessWaitClose(process_executable, 600)
    If ProcessExist(process_executable) {
        ; The testing platform does not like processes that hang
        ProcessClose(process_executable)
    }
}

patient_win_activate_as(process_executable, alternate_title) {
    process_string := Format("ahk_exe {1}", process_executable)
    Loop 1800 {
        ; Greedy Try-Catch for corner cases where the WeChat window is matched but does not activate
        Try {
            WinWait(process_string, , 600)
            WinWait(alternate_title, , 600) ; Making sure this window exists
            WinActivate(process_string)
            Break
        } Catch Any {
            Sleep(1000)
            Continue
        }
    }
}

patient_win_activate(process_executable) {
    process_string := Format("ahk_exe {1}", process_executable)
    Loop 1800 {
        ; Greedy Try-Catch for corner cases where the WeChat window is matched but does not activate
        Try {
            WinWait(process_string, , 600)
            WinActivate(process_string)
            Break
        } Catch Any {
            Sleep(1000)
            Continue
        }
    }
}

win_get_shape(&win_width, &win_height, process_executable) {
    process_string := Format("ahk_exe {1}", process_executable)
    WinGetClientPos(&win_x, &win_y, &win_width, &win_height, process_string) ; Only works when AutoHotKey is run as administrator
}

; Ensure that all instances of WeChat are closed. See chocolateyInstall.ps1
close_all_instances("WeChat.exe")
