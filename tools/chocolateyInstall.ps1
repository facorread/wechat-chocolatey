﻿$PackageVersion = "3.9.8.15"

$Global:ErrorActionPreference = 'Stop'
$Global:InformationPreference = 'Continue'
$Global:VerbosePreference     = 'Continue'
$PackageInstaller             = "WeChatSetup_$PackageVersion"

# The WeChat installer and uninstaller do not detect that an instance of WeChat is running.
# Let us ensure a clean install.

Get-Process -Name WeChat, "$PackageInstaller" , 'Au_' -ErrorAction SilentlyContinue | Stop-Process

$autoHotkeyArgs = @{
    FilePath     = Get-ChocolateyPath -PathType InstallPath | Join-Path -ChildPath "lib\autohotkey.portable\tools\AutoHotkey.exe"
    ArgumentList = Get-ChocolateyPath -PathType PackagePath | Join-Path -ChildPath "tools\install.ahk"
    PassThru     = $true
}

$AutoHotKeyProcess = Start-Process @autoHotkeyArgs
Write-Verbose "Running $($AutoHotKeyProcess.ProcessName)"

$packageArgs = @{
    packageName                   = 'wechat'
    fileType                      = 'exe'
    url                           = "https://webcdn.m.qq.com/spcmgr/download/$PackageInstaller.exe"
    SilentArgs                    = ''
    softwareName                  = 'WeChat*'
    checksum                      = '761F9DE18676B4FE534BB09DD4D1B01CF4EEF31BB6E2B884F30A268FEF0E33634DD3A609BD85218BC33A3600EE97BC68FFD93478D029D7423312AF6CF8263299'
    checksumType                  = 'sha512'
    UseOnlyPackageSilentArguments = $true
}

Install-ChocolateyPackage @packageArgs
Wait-Process -InputObject $AutoHotKeyProcess -ErrorAction SilentlyContinue
Write-Verbose "Process $($AutoHotKeyProcess.ProcessName): HasExited $($AutoHotKeyProcess.HasExited) ExitCode $($AutoHotKeyProcess.ExitCode)"
