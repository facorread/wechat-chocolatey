#SingleInstance Force
#Include "common.ahk"

TrayTip("Chocolatey is using your mouse to go through the uninstaller", "Uninstalling WeChat")
; Ensure that any spurious instances of the WeChat installer are closed. See chocolateyInstall.ps1
close_all_instances(Format("WeChatSetup{1}.exe", package_version))

uninstaller_executable := "Au_.exe"
Sleep 1000
patient_win_activate(uninstaller_executable)
; Send "{Tab}{Space}" ; Not currently working; see install.ahk for more information
win_get_shape(&win_width, &win_height, uninstaller_executable)
Loop {
    Sleep 1000
} Until PixelSearch(&click_x, &click_y, win_width * 0.45, win_height * 0.7, win_width * 0.55, win_height * 0.8, 0x1aad19)
Click(click_x, click_y) ; Click the "Uninstall" button
exe_path := "C:\Program Files\Tencent\WeChat\WeChat.exe"
; The following Loop has a recommended timeout of 1800 seconds
Loop 1800 {
    Sleep 1000
    If FileExist(exe_path) {
        Continue
    } Else {
        Break
    }
}
hide_tray_tip
TrayTip("Giving the uninstaller some extra time", "Uninstalling WeChat")
Sleep 5000
patient_win_activate(uninstaller_executable)
; Send "+{Tab}+{Tab}+{Tab}{Space}" ; Not currently working; see install.ahk for more information
win_get_shape(&win_width, &win_height, uninstaller_executable) ; The window shape just changed
Loop {
    Sleep 1000
} Until PixelSearch(&click_x, &click_y, win_width * 0.7, win_height * 0.7, win_width * 0.8, win_height * 0.8, 0x1aad19)
Click(click_x, click_y) ; Click the "Done" button
patient_process_close("Au_.exe")
