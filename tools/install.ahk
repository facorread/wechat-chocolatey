#SingleInstance Force
#Include "common.ahk"

TrayTip("Chocolatey is using your mouse to go through the installer", "Installing WeChat")
; Ensure that any spurious instances of the WeChat uninstaller are closed. See chocolateyInstall.ps1
close_all_instances("Au_.exe")

installer_executable := Format("WeChatSetup_{1}.exe", package_version)
Sleep(1000)
patient_win_activate_as(installer_executable, "WeChat Install Wizard")
; Sending keystrokes, clicks, and events to the WeChat installer
; Send("{Tab}{Space}+{Tab}{Space}") ; Not currently working; nothing happens
; WinGetControls returns an empty list; the installer keeps its controls private
; We need to use win_width and win_height as the only reference we have available
win_get_shape(&win_width, &win_height, installer_executable)
Loop {
    Sleep 1000
} Until PixelSearch(&click_x, &click_y, win_width * 0.15, win_height * 0.7, win_width * 0.21, win_height * 0.8, 0xffffff)
Click(click_x, click_y) ; Click the "I have read and agreed to Terms and Privacy Policy" checkbox
Sleep(1000)
Loop {
    Sleep 1000
} Until PixelSearch(&click_x, &click_y, win_width * 0.45, win_height * 0.6, win_width * 0.55, win_height * 0.7, 0x07c160)
Click(click_x, click_y) ; Click the "Install" button
exe_path := "C:\Program Files\Tencent\WeChat\WeChat.exe"
Loop 1800 {
    Sleep(1000)
    If not FileExist(exe_path) {
        Continue
    }
    If FileGetVersion(exe_path) == package_version {
        Break
    }
}
hide_tray_tip
TrayTip("Giving the installer some extra time", "Installing WeChat")
Sleep(5000)
patient_win_activate_as(installer_executable, "WeChat Install Wizard")
; Send("{Tab}{Tab}{Tab}{Space}") ; Not currently working; we will use the mouse instead
MouseMove(win_width * 0.1, win_height * 0.1) ; If the Run button appears under the mouse pointer, this ends the hovering and returns the color to default
Loop {
    Sleep 1000
} Until PixelSearch(&click_x, &click_y, win_width * 0.45, win_height * 0.6, win_width * 0.55, win_height * 0.7, 0x07c160)
Click(click_x, click_y) ; Click the "Run" button: it will close the installer, start WeChat, avoid hangs.
patient_win_activate("WeChat.exe")
close_all_instances("WeChat.exe")
hide_tray_tip
